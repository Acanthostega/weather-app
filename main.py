#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from kivy.app import App


class WeatherApp(App):
    """
    The main class for the weather application.
    """


if __name__ == '__main__':
    WeatherApp().run()


# vim: set tw=79 :
